Как и в большинстве языков программирования, в Bash есть циклы. Они бывают двух видов:

- for — когда количество итераций заранее известно.
- while — когда количество итераций заранее не известно.

Вот как выглядит сигнатура цикла for:

``` bash 
for переменная цикла in элемент1 элемент2 ... элементN 
do
# Команды, которые будут выполняться в цикле 
done 
```  

Количество итераций (часть между in и do) можно указать простым перечислением элементов:
``` bash 
for fruit in apple banana cherry
do
    echo "I like $fruit"
done

# Будет выведено:
# I like apple
# I like banana
# I like cherry
```


Можно перебрать элементы массива:
``` bash 
my_array=("apple" "banana" "orange")

for fruit in "${my_array[@]}"
do
    echo "I like $fruit"
done

# Будет выведено:
# I like apple
# I like banana
# I like cherry
```

Для явного указания нужного количества итераций используют арифметические выражения:
``` bash 
for ((i=0; i<5; i++))
do
  echo "Iteration number $i"
done
# Будет выведено:
# Iteration number 0
# Iteration number 1
# Iteration number 2
# Iteration number 3
# Iteration number 4
```


А вот так выглядит сигнатура цикла while:
``` bash 
while [ условие ]
do
  # Команды, которые будут выполняться в цикле
done
```


**Условие** **(condition)** — это выражение, результат которого является логическим значением true или false. Команды внутри цикла будут выполняться до тех пор, пока condition возвращает true.

Давайте усовершенствуем нашу программу проверки пароля так, чтобы она запрашивала его до тех пор, пока пользователь не введёт верный:
``` bash 
#!/bin/bash
password="14pomTEr"
user_input=""

while [ "$user_input" != "$password" ]
do
  read -p "Enter the password: " user_input
  echo "Wrong password, login denied"
done

echo "Password accepted, login allowed"
``` 

**Что здесь происходит.** В этом примере цикл while запрашивает пароль у пользователя, а потом проверяет его на совпадение с правильным с помощью оператора ["$user_input" != "$password"]. А дальше происходит следующее:

- Если пароль неверный, программа выводит соответствующее сообщение, а цикл продолжается.
- Если пароль верный, программа выводит сообщение о принятии пароля и завершает выполнение.
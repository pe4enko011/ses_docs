* Tell info
	 * **git config --global user.name "SOme Name"
	 * **git config --global user.email "som@em.ru"
* Work with repo
	 * git init - create repo
	 * git clone path_or_url - clone repo from somewhere
* Add files to track changes
	 * git add file_name
	 * git add * - will add all
	 * git commit -m "feat: implement some feature"
	 * git commit -a - commit all
	 * git push origin master - origin(repo) master(branch)
* Check status 
	 * git status
* Connect to remote repo
	 * git remote add origin server:path_to_project
	 * git remote -v  - will show all remotes repo
* Branches
	 * git branch - show current branch
	 * git checkout -b branch_name - create and switch to new branch
	 * git checkout branch_name - change current branch
	 * git branch -d branch_name - delete branch
	 * git push origin branch_name - will create on remote server new branch and put there your branch
* Update information from server
	 * git fetch --all
	 * git pull - get changes from server
	 * git merge branch_name - merge changes
	 * git diff - shows merge conflicts
	 * git diff --base file_name
	 * git diff source_branch target_branch
* Tags
	 * git tag 1.0.0 commit_id - create a tag
* Logs
	 * git log
	 * git status
* Search 
	 * git grep "foo()"
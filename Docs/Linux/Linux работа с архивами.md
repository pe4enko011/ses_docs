#### Создать tar-архив с именем primer.tar содержащий /home/primer.txt;
``` bash 
tar cf primer.tar /home/primer.txt
```
#### Cоздать tar-архив с сжатием Gzip по имени primer.tar.gz;
``` bash 
tar czf primer.tar.gz /home/primer.txt
```
#### Cоздать tar-архив с сжатием Bzip2 по имени primer.tar.bz;
``` bash 
tar cjf primer.tar.bz2 /home/primer.txt
```
#### Распаковать архив primer.tar в текущую папку;
``` bash 
tar xf primer.tar
```
#### Распаковать tar-архив с Gzip;
``` bash 
tar xzf primer.tar.gz
```
#### Распаковать tar-архив с Bzip2;
``` bash 
tar xjf primer.tar.bz
```
- #### Показать версию ядра Linux
>**<font size=5 color = "red">uname </font>**

- #### Очищение экрана терминала
>**<font size=5 color = "red">clear</font>

- #### Показывает текущую дату и время;
>**<font size=5 color = "red">date</font>

- #### Показывает в удобной форме предыдущий, текущий и последующий месяц ;
>**<font size=5 color = "red">cal -3</font>

- #### Показать текущее время и работу системы без перезагрузки и выключения;
>**<font size=5 color = "red">uptime</font>


- ####  Показать сетевое имя компьютера;
>**<font size=5 color = "red">hostname</font>

- #### Показать информацию о доменом имени linux.org;
>**<font size=5 color = "red">whois linux.org</font>

- #### Изменить переменной окружения http_proxy, для использования интернета через proxy-сервер;
>**<font size=5 color = "red">export http_proxy=http://your.proxy:port</font>

- #### Скачать файл https://itshaman.ru/images/logo_white.png в текущую папку;
>**<font size=5 color = "red">wget https://itshaman.ru/images/logo_white.png</font>

- ####  Копирование сайта целиком и конвертирование ссылок для автономной работы. Копирование происходит на 5 уровней в глубину;
>**<font size=5 color = "red">wget —convert-links -r http://www.linux.org/</font>

- #### Создание и настройка Dial-Up соединения для выхода в Интернет по модему;
>**<font size=5 color = "red">pppconfig</font>

- #### Выполнить последнюю команду;
>**<font size=5 color = "red">!!</font>

- #### Показать последние 50 набранных команд;
>**<font size=5 color = "red">history | tail -50</font>
- #### Завершить сеанс текущего пользователя;
>**<font size=5 color = "red">exit</font>

- #### Меняет пароль текущего пользователя;
>**<font size=5 color = "red">passwd</font>

- #### Завершение работы Linux;
>**<font size=5 color = "red">shutdown -h now</font>

- #### Выключение машины
>**<font size=5 color = "red">poweroff</font>

- #### Перезагрузка системы;
>**<font size=5 color = "red">reboot</font>

- #### Cтатистика перезагрузок;
>**<font size=5 color = "red">last reboot</font>

- #### Показывает IP-адрес введенного сайта;
>**<font size=5 color = "red">host itshaman.ru</font>

